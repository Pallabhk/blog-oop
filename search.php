<?php include 'inc/header.php';?>
<?php include 'inc/slider.php';?>
<?php include 'config/config.php';?>
<?php include 'lib/Database.php';?>
<?php include 'helpers/Formate.php';?>

<!--Create Object-->

<?php
	
	$db = new Database();
	$fm = new Formate();

	if (!isset($_GET['search']) || $_GET['search']== NULL) {
		header("Location: 404.php");
	}else{
		$search =$_GET['search'];
	}
?>
	

	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
			<?php
				$query ="SELECT * FROM tbl_post WHERE title LIKE '%$search%' OR body LIKE '%$search%'";
				$post  =$db->select($query);
				if ($post) {
				while ($result = $post->fetch_assoc()) { ?>
				
				<div class="samepost clear">
					<h2><a href="post.php?id=<?php echo $result['id'];?>"><?php echo $result['title'];?></a></h2>

					<h4><?php echo $fm->formateDate($result['date']);?> By <a href="#"><?php echo $result['author'];?></a></h4>
					 <a href="#"><img src="admin/upload/<?php echo $result['image'];?>" alt="post image"/></a>
					<?php echo $fm->readMore($result['body']);?>

					<div class="readmore clear">
						<a href="post.php?id=<?php echo $result['id'];?>">Read More</a>
					</div>
				</div>
				<?php } }else{?>
					<p>Your search option is not found</p>
				<?php }?>
		</div>

	<?php include 'inc/sidebar.php';?>

	<?php include 'inc/footer.php';?>