<?php include 'inc/header.php';?>
<?php include 'config/config.php';?>
<?php include 'lib/Database.php';?>
<?php include 'helpers/Formate.php';?>

<?php
	
	$db = new Database();
	$fm = new Formate();

	if(!isset($_GET['id']) || $_GET['id'] == NULL){
		header("Location:404.php");
	}else{
		$id = $_GET['id'];
	}
?>
	<div class="contentsection contemplete clear">
		<div class="maincontent clear">
			<div class="about">
				<?php
					
					$query ="SELECT * FROM tbl_post where id=$id";
					$post  = $db->select($query);

					if ($post) {
						while ($result =$post->fetch_assoc()) {?>
						
						
					<h2><?php echo $result['title']?></h2>
					<h4><?php echo $fm->formateDate($result['date']);?> By <?php echo $result['author'];?></h4>
					<img src="<?php echo $result['image'];?>" alt="MyImage"/>

					<?php echo $result['body'];?>

					<?php  ?>
					
				
				<div class="relatedpost clear">
					<?php
					$catid    = $result['cat'];
					$querycat ="SELECT * FROM tbl_post WHERE cat='$catid' ORDER BY rand() LIMIT 6";
					$post     =$db->select($querycat);
					if ($post) {
						while ($reresult =$post->fetch_assoc()) {?>
				
					
					<h2>Related articles</h2>
					<a href="post.php?id=<?php echo $reresult['id'];?>">
						<img src="upload/<?php echo $reresult['image'];?>" alt="post image"/>
					</a>
					<?php }  }?>
				</div>
				<?php } }else{header("Location:404.php");}?>
			</div>
		
		</div>
		<?php include 'inc/sidebar.php';?>
		
	    <?php include 'inc/footer.php';?>